class motor_control:
    """Motor controller for h-bridge motor controllers"""
    def __init__(self, board, dir, cdir, pwm):
        self.board = board
        self.dir = self.board.digital[dir]
        self.cdir = self.board.digital[cdir]
        self.pwm = self.board.get_pin('d:{}:p'.format(pwm))

    def forward(self, speed):
        self.dir.write(1)
        self.cdir.write(0)
        self.pwm.write(speed)

    def reverse(self, speed):
        self.dir.write(0)
        self.cdir.write(1)
        self.pwm.write(speed)

    def stop(self):
        self.dir.write(0)
        self.dir.write(0)
        self.pwm.write(0)
